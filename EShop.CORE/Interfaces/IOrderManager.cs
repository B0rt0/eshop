﻿using EShop.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    public interface IOrderManager : IManager<Order>
    {
        /// <summary>
        /// Añade una linea de producto al carrito.
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <param name="sessionId">Id de la sesion</param>
        /// <param name="productId">Id del producto</param>
        /// <param name="quantity">Cantidad del producto</param>
        /// <returns></returns>
        decimal AddProduct(string userId, string sessionId, int productId, decimal quantity);

        /// <summary>
        /// Obtiene el carrito del usuario
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <param name="sessionId">Id de sesion</param>
        /// <returns></returns>
        IQueryable<Order> GetShoppingCartByUser(string userId, string sessionId);
    }
}
