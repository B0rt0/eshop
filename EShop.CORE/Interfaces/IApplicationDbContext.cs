﻿using System.Data.Entity;
using EShop.CORE;
using EShop.CORE.Domain;
using System.Data.Entity.Infrastructure;

namespace EShop.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del contexto de datos
    /// </summary>
    public interface IApplicationDbContext
    {
        /// <summary>
        /// Coleccion persistible de categorias
        /// </summary>
        DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Coleccion persistible de pedidos
        /// </summary>
        DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Coleccion persistible de productos
        /// </summary>
        DbSet<Product> Products { get; set; }

        /// <summary>
        /// Coleccion persistible de carrito
        /// </summary>
        DbSet<ShoppingCartLines> ShoppingCart { get; set; }

        /// <summary>
        /// Coleccion persistible impuestos 
        /// </summary>
        DbSet<Tax> Taxes { get; set; }

        /// <summary>
        /// Metodo para giuardar cambios
        /// </summary>
        /// <returns>Registros afectados</returns>
        int SaveChanges();

        /// <summary>
        /// Obtiene la coleccion de una entidad
        /// </summary>
        /// <typeparam name="T">Entidad de la que queremos el contexto</typeparam>
        /// <returns>Coleccion de la entidad</returns>
        DbSet<T> Set<T>() where T : class;

        /// <summary>
        /// Obtiene una entrada de una entidad del contexto
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de la que queremos la entrada</typeparam>
        /// <param name="entity">Entidad de la que queremos su entrada</param>
        /// <returns>Entrada de la entidad</returns>
        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}