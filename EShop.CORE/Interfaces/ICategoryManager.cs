﻿using EShop.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager de categorias
    /// </summary>
    public interface ICategoryManager : IManager<Category>
    {
    }
}
