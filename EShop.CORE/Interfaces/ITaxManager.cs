﻿using EShop.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    public interface ITaxManager : IManager<Tax>
    {
        /// <summary>
        /// Calcula el eprecio final de un producto
        /// </summary>
        /// <param name="taxId">Tasas</param>
        /// <param name="price">+Precio base</param>
        /// <returns>Precio finall</returns>
        decimal GetFinalPrice(int taxId, decimal price);

        /// <summary>
        /// Devuelve el precio del producto neto
        /// </summary>
        /// <param name="taxId">Id de la tasa</param>
        /// <param name="price">Precio bruto</param>
        /// <returns>Precio del producto</returns>
        decimal GetNetPrice(int taxId, decimal price);

        /// <summary>
        /// Calcula el precio neto de una cantidad
        /// </summary>
        /// <param name="tax">Valor de la tasa</param>
        /// <param name="price">Precio bruto</param>
        /// <returns>Precio sin tasas</returns>
        decimal GetNetPrice(decimal tax, decimal price);
        
    }
}
