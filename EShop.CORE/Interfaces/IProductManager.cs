﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager de producto
    /// </summary>
    public interface IProductManager : IManager<Product>
    {
        /// <summary>
        /// Obtiene los productos por categoria
        /// </summary>
        /// <param name="categoryId">Identificador</param>
        /// <returns>Coleccion de produ ctos que coincidan con la categoria</returns>
        IQueryable<Product> GetByCategoryId(int categoryId);
    }
}
