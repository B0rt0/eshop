﻿using EShop.CORE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    /// <summary>
    /// Manager del carrito de la compra
    /// </summary>
    public interface IShoppingCartManager : IManager<ShoppingCartLines>
    {
        /// <summary>
        /// Añade productos al carrito
        /// </summary>
        /// <param name="userId">Id del usuario</param>
        /// <param name="sessionId">session del usuari</param>
        /// <param name="productId">Id del producto</param>
        /// <param name="quantity">Cantidad del producto</param>
        /// <returns></returns>
        decimal AddProduct(string userId, string sessionId, int productId, decimal quantity);

        /// <summary>
        /// Obtiene el carrito del usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        IQueryable<ShoppingCartLines> GetShoppingCartByUser(string userId, string sessionId);
    }
}
