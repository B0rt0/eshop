﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Interfaces
{
    /// <summary>
    /// Interfaz del manager generico
    /// </summary>
    public interface IManager<T>
    {

        

        /// <summary>
        /// Obtiene un elemento por su ID
        /// </summary>
        /// <param name="id"> Identificador</param>
        /// <returns>Objeto con el id indicado </returns>
        T GetById(object id);

        /// <summary>
        /// Obtiene todos los registros
        /// </summary>
        /// <returns>Lista de todos los registros</returns>
        IQueryable<T> GetAll();

        ///<sumary>
        /// Recibe el contexto de la aplicación
        /// </summary>
        IApplicationDbContext Context
        {
            get;
        }
        /// <summary>
        /// Devuelve las ids de una lista de objetes que corresponden a la categoria.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
            IEnumerable<T> GetByIds(List<Object> ids);

        /// <summary>
        /// Metodo para eliminr elementos
        /// </summary>
        /// <param name="element"></param>
        void Remove(T element);
        
            /// <summary>
            /// Guardar cambios
            /// </summary>
            /// <returns>Numero de registros afectados</returns>
        int SaveChanges();

        /// <summary>
        /// Añade un elemento a la coleccion
        /// </summary>
        /// <param name="element"></param>
        void Add(T element);


    }
}
