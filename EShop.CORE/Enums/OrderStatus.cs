﻿namespace EShop.CORE.Enums
{
    public enum OrderStatus
    {
        Pending,
        Paid,
        InProcess,
        Send,
        End,
        Canceled,
    }
}