﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Enums
{
    public enum DocumentType
    {
        Image,
        Video,
        Pdf,
        Word,
    }
}
