﻿using EShop.CORE.Domain;
using EShop.CORE.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE
{
    /// <summary>
    /// Clase de dominio de producto
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion corta
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        public decimal Stock { get; set; }

        /// <summary>
        /// Disposicion del producto
        /// </summary>
        public bool Availabel { get; set; }

        /// <summary>
        /// Impuesto asociado
        /// </summary>
        public Tax Tax { get; set; }
        [ForeignKey ("Tax")]
        public int Tax_Id { get; set; }

        /// <summary>
        /// Categorias de productos
        /// </summary>
        public List<Category> Categories { get; set; }

        /// <summary>
        /// Documentos del producto 
        /// </summary>
        public List<Document> Documents { get; set; }

        /// <summary>
        /// El producto es destacado
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// Indica el porcentaje de descuento del producto si lo tiene
        /// </summary>
        public decimal? Discount { get; set; }
    }
}
