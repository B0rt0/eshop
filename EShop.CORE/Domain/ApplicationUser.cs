﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EShop.CORE.Domain

{
    public class ApplicationUser : IdentityUser
    {

        /// <summary>
        /// Metodo para crear usuarios asincronamente
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="authenticationType">Tipo de autentificacion</param>
        /// <returns></returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }

        /// <summary>
        /// Direcciones de usuario
        /// </summary>
        public List<Address> Address { get; set; }

        /// <summary>
        /// Apellidos del usuario
        /// </summary>
        [Required]
        public string Surnames { get; set; }

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        [Required]
        public string Name { get; set; }
        
        
        /// <summary>
        /// Documento de identidad
        /// </summary>
        [MaxLength(10)]
        [Required]
        [MinLength(7)]
        public string NIF { get; set; }
    }
}