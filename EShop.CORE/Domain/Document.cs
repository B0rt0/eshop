﻿using EShop.CORE.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain
{
    public class Document
    {
        /// <summary>
        /// Id del del documento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Localizacion del documento
        /// </summary>
        public string Path { get; set; }

        public DocumentType Type { get; set; }

        public int Order { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

         public Product Product { get; set; }
    }
}
