﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain
{
        public class Category
    {
        /// <summary>
        /// Id de la categoria
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la categoria
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Identificador de la categoria padre
        /// </summary>
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        /// <summary>
        /// Objeto de la categoria padre
        /// </summary>
        public Category Parent { get; set; }

        /// <summary>
        /// Coleccion de productos
        /// </summary>
        public List<Product> Products { get; set; }
    }
}
