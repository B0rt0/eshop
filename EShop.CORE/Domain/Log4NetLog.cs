﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain
{
    [Table("Log4NetLog")]
    /// <summary>
    /// Clase de dominio del log
    /// </summary>
    public class Log4NetLog
    {
        
        /// <summary>
        /// Idemtificador del log 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Fecha del log
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Numero de hilo de ejecucion
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string Thread { get; set; }
        /// <summary>
        /// Nivel del log
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Level { get; set; }
        /// <summary>
        /// Persona que causo el log
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Logger { get; set; }
        /// <summary>
        /// Mensage del log
        /// </summary>
        [Required]
        [MaxLength(4000)]
        public string Message { get; set; }
        /// <summary>
        /// Excepcion del log
        /// </summary>
        [Required]
        [MaxLength(2000)]
        public string Exception { get; set; }
    }
}
