﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain

{
    public class ShoppingCartLines
    {
        /// <summary>
        /// Identificador de carro
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        /// <summary>
        /// Usuario
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Sesion usuario
        /// </summary>
        public string Session { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Identificador del producto
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        /// Producto del carrito
        /// </summary>
        public Product Product { get; set; }


        /// <summary>
        /// Cantidad del producto
        /// </summary>
        public decimal Quantity { get; set; }
    }
}
