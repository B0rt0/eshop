﻿using EShop.CORE.Domain;
using EShop.CORE.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain

{
    public class Order
    {
        /// <summary>
        /// Identificador de pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Coleccion de lineas de pedido
        /// </summary>
        public List<OrderLine> OrderLines { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Fecha de creacion del pedido
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha en la que el pedido fue abonado
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Fecha de expedicion
        /// </summary>
        public DateTime? ShippmentDate { get; set; }

        /// <summary>
        /// Fecha de llegada
        /// </summary>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Fecha de cancelacion
        /// </summary>
        public DateTime? CancellationDate { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        
        /// <summary>
        /// Usuario
        /// </summary>
        public ApplicationUser User { get; set; }
        
        /// <summary>
        /// Identificador de la direccion de envio
        /// </summary>
        [ForeignKey("DeliveryAddres")]
        public int DeliveryAddressId { get; set; }

        /// <summary>
        /// Direccion de recepcion
        /// </summary>
        public Address DeliveryAddres { get; set; }

        /// <summary>
        /// ID Direccion de facturacion 
        /// </summary>
        [ForeignKey("BillingAddress")]
        public int BillingAddressId { get; set; }

        /// <summary>
        /// Direccion de facturacion
        /// </summary>
        public Address BillingAddress{ get; set; }

        /// <summary>
        /// Metodo de envio
        /// </summary>
        public string ShippmenthMethod { get; set; }

        /// <summary>
        /// Metodo de pago
        /// </summary>
        public string PaymenthMethod { get; set; }
    }
}
