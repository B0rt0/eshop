﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.CORE.Domain
{
    public class OrderLine
    {
        public int Id { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        /// <summary>
        /// Usuario
        /// </summary>
        public Order Order { get; set; }

        public int ProductId { get; set; }

        public string  ProductName { get; set; }

        public string TaxName { get; set; }

        public string TaxValue { get; set; }

        public decimal Quantity { get; set; }
        
        public decimal ProductPrice { get; set; }

        public decimal TotalPrice { get; set; }

    }
}
