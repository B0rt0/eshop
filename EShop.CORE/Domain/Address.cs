﻿using EShop.CORE.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop.CORE.Domain
{
    public class Address
    {
        public int Id { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        /// <summary>
        /// Usuario
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Direccion numero 1
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Direccion numero 2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Pais
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Provuncia
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Ciudad
        /// </summary>
        public string City { get; set; }
    }
}