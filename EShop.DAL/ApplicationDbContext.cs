﻿using EShop.CORE;
using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DAL
{
    //Add-Migration "Featured" -StartUpProjectName "EShop.API" -ConnectionStringName "DefaultConnection" -Verbose
    //Update-Database -StartUpProjectName "EShop.API" -ConnectionStringName "DefaultConnection" -Verbose

    /// <summary>
    /// Contexto de datos de EntityFramework
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        /// <summary>
        /// Constructor para la migracion
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {

        }
        /// <summary>
        /// Constructor cadena de conexion
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        public ApplicationDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {

        }
        #region coleciones persistentes
        /// <summary>
        /// Coleccion persistible de pedidos
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Coleccion persistible de categorias
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Coleccion persistible de productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Logs de la app
        /// </summary>
        public DbSet<Log4NetLog> Log4NetLog { get; set;}
           

        /// <summary>
        /// Coleccion persistible de lineas de carro
        /// </summary>
        public DbSet<ShoppingCartLines> ShoppingCart { get; set; }

        /// <summary>
        /// Coleccion persistible de impuestos
        /// </summary>
        public DbSet<Tax> Taxes { get; set; }
        #endregion

        /// <summary>
        /// Excepciones en la base de datos
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Quitar el borrado en cascada entre orden y direccion de la facturacion
            //Para evitar las referencias ciclicas
            modelBuilder.Entity<Order>().HasRequired(f => f.BillingAddress)
                .WithMany().WillCascadeOnDelete(false);

            //Quitar el borrado en cascada entre orden y direccion de envio
            //para evitar referencias ciclicas
            modelBuilder.Entity<Order>().HasRequired(f => f.DeliveryAddres)
                .WithMany().WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }


    }
}
