﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DTOs
{
    /// <summary>
    /// DTO de producto
    /// </summary>
    public class ProductDTO
    {
        /// <summary>
        /// ID del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id del nombre
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Imagen del producto
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Precio final del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Precio sin desuento
        /// </summary>
        public  decimal? OldPrice { get; set; }

        /// <summary>
        /// Si esta de oferta
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

    }
}
