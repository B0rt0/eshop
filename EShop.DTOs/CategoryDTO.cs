﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DTOs
{
    /// <summary>
    /// DTO de categorias
    /// </summary>
    public class CategoryDTO
    {
        /// <summary>
        /// Id de la categoria
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la categoria
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Objeto de la categoria padre
        /// </summary>
        public int ParentId { get; set; }

     
    }
}
