﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DTOs
{
    /// <summary>
    /// DTO de lineas de carrito
    /// </summary>
    public class ShoppingCartLinesDTO
    {
        /// <summary>
        /// Identificador de las lineas de carrito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// CantidAD
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Precio unidad con impuestos incluidos
        /// </summary>
        public decimal Price { get; set; }
    }
}
