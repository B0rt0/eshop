﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DTOs
{
    /// <summary>
    /// DTO de pedido
    /// </summary>
    public class OrderDTO
    {
        /// <summary>
        /// Id de la linea de carrito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cantidad del producto
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Precio de la unidad del producto con impuestos
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        ///  Precio total con Cantidad x Precio. Impuestos incluidos.
        /// </summary>
        public decimal TotalPrice { get; set; }
    }
}
