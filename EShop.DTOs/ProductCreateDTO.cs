﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.DTOs
{/// <summary>
/// Dto para crear y editar productos
/// </summary>
    public class ProductCreateDTO : ProductDTO
    {
        /// <summary>
        /// identificador del impuesto
        /// </summary>
        public int Tax_Id { get; set; }
        /// <summary>
        /// Categorias
        /// </summary>
        public List<int> Categories { get; set; }
    }
}
