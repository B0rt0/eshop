﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
namespace EShop.IFR.Helpers
{
    /// <summary>
    /// Helper de Ioc
    /// </summary>
    public static class IoCHelper
    {
        /// <summary>
        /// Resolver un tipo
        /// </summary>
        /// <typeparam name="T">Tipo a resolver</typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            return IoC.UnityConfig.Container.Resolve<T>();
        }
    }
}
