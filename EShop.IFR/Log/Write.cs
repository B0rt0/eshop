﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.IFR.Log
{
    /// <summary>
    /// Log de escritura
    /// </summary>
    public static class Write
    {
        private static ILog log = null;
        public static ILog Log
            {
                get
                {
                    if(log == null)
                    {
                    log4net.Config.XmlConfigurator.Configure();
                    log = LogManager.GetLogger("AdoNetAppender");

                    }
                    return log;
                }
            }
        /// <summary>
        /// Escribe un error en el log
        /// </summary>
        /// <param name="message">Mnesage de error </param>
        /// <param name="ex">Excepcion producida</param>
        public static void Error(string message, Exception ex = null)
        {
            Log.Error(message, ex);
        }

        /// <summary>
        /// Escribe la info en el log
        /// </summary>
        /// <param name="message">Mensage de info</param>
        /// <param name="ex">Excepcion producida</param>
        public static void Info ( string message, Exception ex = null)
        {
            Log.Info(message, ex);
        }

        /// <summary>
        /// Escribe un error fatal en el log
        /// </summary>
        /// <param name="message">Mnesage de error </param>
        /// <param name="ex">Excepcion producida</param>
        public static void Fatal(string message, Exception ex = null)
        {
            Log.Fatal(message, ex);
        }

        /// <summary>
        /// Escribe un warn en el log
        /// </summary>
        /// <param name="message">Mnesage de error </param>
        /// <param name="ex">Excepcion producida</param>
        public static void Warn(string message, Exception ex = null)
        {
            Log.Warn(message, ex);
        }

        /// <summary>
        /// Escribe la info de debug en el log
        /// </summary>
        /// <param name="message">Mensage de Debug</param>
        /// <param name="ex">Excepcion producida</param>
        public static void Debug(string message, Exception ex = null)
        {
            Log.Debug(message, ex);
        }
    }
}
