using System;
using System.Configuration;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;

namespace EShop.IFR.IoC
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"];

            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.IApplicationDbContext, EShop.CORE"),
                Type.GetType("EShop.DAL.ApplicationDbContext, EShop.DAL"),
                new PerRequestLifetimeManager(),
                new InjectionConstructor(connectionString.ToString()));

            

            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.IProductManager, EShop.CORE"),
                Type.GetType("EShop.Application.ProductManager, EShop.Application"));

            container.RegisterType(
               Type.GetType("EShop.CORE.Interfaces.ICategoryManager, EShop.CORE"),
               Type.GetType("EShop.Application.CategoryManager, EShop.Application"));

            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.ICategoryManager, EShop.CORE"),
                Type.GetType("EShop.Application.CategoryManager, EShop.Application"));


            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.IOrderManager, EShop.CORE"),
                Type.GetType("EShop.Application.OrderManager, EShop.Application"));


            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.IShoppingCartManager, EShop.CORE"),
                Type.GetType("EShop.Application.ShoppingCartManager, EShop.Application"));


            container.RegisterType(
                Type.GetType("EShop.CORE.Interfaces.ITaxManager, EShop.CORE"),
                Type.GetType("EShop.Application.TaxManager, EShop.Application"));
        }
    }
}