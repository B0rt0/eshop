﻿using AutoMapper;
using EShop.CORE;
using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using EShop.IFR.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop.API
{
    /// <summary>
    /// Configuracion del autoMapper
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Metodo para configurar el autoMapper
        /// </summary>
        public static void Configure()
        {
            var taxManager = IoCHelper.Resolve<ITaxManager>();
            var categoryManager = IoCHelper.Resolve<ICategoryManager>();
            Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<ShoppingCartLines, ShoppingCartLinesDTO>()
                    .ForMember(dest => dest.Name,
                                opt => opt.MapFrom(src => src.Product.Name)
                        )

                    .ForMember(dest => dest.Price,
                                opt => opt.MapFrom(src => src.Product.Discount == null
                                        ? taxManager.GetFinalPrice(src.Product.Tax_Id, src.Product.Price)
                                        : (decimal?)taxManager.GetFinalPrice(src.Product.Tax_Id,
                                        taxManager.GetNetPrice((decimal)src.Product.Discount, src.Product.Price)))
                        );

                cfg.CreateMap<ProductDTO, Product>();
                cfg.CreateMap<ProductCreateDTO, Product>()
                    .ForMember(dest => dest.Categories,
                    opt => opt.MapFrom(src => src.Categories == null
                            ? null
                            : categoryManager.GetByIds(
                                src.Categories.Select(i => (object)i).ToList()
                            )));
                cfg.CreateMap<Product, ProductDTO>()
                    .ForMember(dest => dest.Price,
                                opt => opt.MapFrom(src =>
                                    src.Discount == null || src.Discount == 0
                                        ? taxManager.GetFinalPrice(src.Tax_Id, src.Price)
                                        : (decimal?)taxManager.GetFinalPrice(src.Tax_Id,
                                        taxManager.GetNetPrice((decimal)src.Discount, src.Price))))

                    .ForMember(dest => dest.OldPrice,
                                opt => opt.MapFrom(src =>
                                    src.Discount == null || src.Discount == 0
                                        ? null
                                        : (decimal?)taxManager.GetFinalPrice(src.Tax_Id, src.Price)))

                    .ForMember(dest => dest.Image,
                                opt => opt.MapFrom(src =>
                                    src.Documents.Where(i => i.Type == CORE.Enums.DocumentType.Image).Any()
                                    ? src.Documents.Where(i => i.Type == CORE.Enums.DocumentType.Image).OrderBy(i => i.Order)
                                        .First().Path
                                    : "")
                              );
                cfg.CreateMap<Tax, TaxDTO>();
            });
        }

    }
}