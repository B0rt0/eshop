﻿using EShop.API.Helper;
using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using EShop.IFR.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EShop.API
{
    /// <summary>
    /// Clase de configuracion de seguridad (usuarios y roles iniciales)
    /// </summary>
    public static class SecurityConfig
    {
        private const string RolAdmin = "Admin";
        private const string UserAdmin = "admin@admin.com";
        private const string PwdAdmin = "123Asd!";
        /// <summary>
        /// Configurar el usuario admin y roles
        /// </summary>
        public static void Configure()
        {
            //Configuracion de seguridad
            var urManager = new UserRolManager();

            //su no existe admin lo creas
            if (!urManager.RoleManager.RoleExists(RolAdmin))
                urManager.RoleManager.Create(new IdentityRole(RolAdmin));
            
            //Crea el usuario admin
            ApplicationUser user = urManager.UserManager.FindByName(UserAdmin);

            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = UserAdmin,
                    NIF = "0000000",
                    Name = "Super",
                    Surnames = "Admin",
                    Email = UserAdmin,
                };
                try
                {
                    IdentityResult result = urManager.UserManager.Create(user, PwdAdmin);
                    if (result.Succeeded)
                    {
                        urManager.UserManager.AddToRole(user.Id, RolAdmin);
                    }
                    else
                    {
                        //TODO: Escribir en un log
                        throw new Exception(string.Join(",", result.Errors));
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Escribir log
                    throw ex;
                }
            }
            else
            {
                if (!urManager.UserManager.IsInRole(user.Id, RolAdmin))
                {
                    urManager.UserManager.AddToRole(user.Id, RolAdmin);
                }
            }
            }
        }
    }
