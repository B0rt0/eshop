﻿using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using EShop.IFR.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EShop.API.Helper
{    
    public class UserRolManager
    {
        public UserManager<ApplicationUser> UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; private set; }
        public IApplicationDbContext Context { get; private set; }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public UserRolManager(IApplicationDbContext context =null)
        {
            if (context != null)

                Context = context;
            else
                Context = IoCHelper.Resolve<IApplicationDbContext>();
            RoleManager = new RoleManager<IdentityRole>
                (new RoleStore<IdentityRole>((DbContext)Context));

            UserManager = new  UserManager<ApplicationUser>
                (new UserStore<ApplicationUser>((DbContext)Context));
        }
    }
}