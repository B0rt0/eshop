﻿using AutoMapper.QueryableExtensions;
using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop.API.Controllers
{
    /// <summary>
    /// Controlador de la tienda
    /// </summary>
    [RoutePrefix("api/Shop")]
    public class ShopController : ApiController
    {

        IShoppingCartManager cartManager;
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="cartManager">Manager del carrito</param>
        public ShopController(IShoppingCartManager cartManager)
        {
            this.cartManager = cartManager;
        }

        /// <summary>
        /// Metodo para añadir producto al carrito.
        /// </summary>
        /// <param name="productId">Id del prpducto</param>
        /// <param name="quantity">Cantidad a añadir</param>
        /// <param name="sessionId">Identificador de sesion</param>
        /// <returns>Elementos que tiene el carrito</returns>
        [HttpPost]
        [Route("AddProduct/{productId}/{quantity}/{sessionId}")]
        public decimal AddProduct(int productId, int quantity, string sessionId)
        {
            return cartManager.AddProduct(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
            sessionId,
            productId,
            quantity);

        }

        /// <summary>
        /// Obtiene el carrito del usuario
        /// </summary>
        /// <param name="sessionId">Identificador del usuario o de sesion</param>
        /// <returns>Lista de lineas del carrito</returns>
        [HttpGet]
        [Route("Cart/{sessionId}")]
        public IEnumerable<ShoppingCartLinesDTO> GetCart(string sessionId)
        {
            return cartManager.GetShoppingCartByUser(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId
            ).ToList().AsQueryable().ProjectTo<ShoppingCartLinesDTO>().ToList();
        }

        /// <summary>
        /// Elimina una linea del carrito
        /// </summary>
        /// <param name="id">Id de la liea a borrar</param>
        /// <param name="sessionId">Session </param>
        [HttpDelete]
        [Route("Cart/{id}/{sessionId}")]
        public void Delete (int id, string sessionId)
        {
            var line = cartManager.GetShoppingCartByUser(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId
                ).Where(e => e.Id == id).SingleOrDefault();

            if(line!=null)
            {
                cartManager.Remove(line);
                cartManager.SaveChanges();
            }
            else
            {
                throw new KeyNotFoundException();
            }

        }
    }
}
