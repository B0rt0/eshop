﻿using AutoMapper.QueryableExtensions;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop.API.Controllers
{
    /// <summary>
    /// Controlador de impuestos
    /// </summary>
    [RoutePrefix("api/Tax")]
    public class TaxController : ApiController
    {
        ITaxManager taxManager = null;
        public TaxController(ITaxManager taxManager)
        {
            this.taxManager = taxManager;
        }

        /// <summary>
        /// Obtiene todos los ivas
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public IEnumerable<TaxDTO> GetAll()
        {
            return taxManager.GetAll().ProjectTo<TaxDTO>();
        }
    }
}
