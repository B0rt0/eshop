﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EShop.API.Extensions;
using EShop.CORE;
using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using EShop.IFR.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop.API.Controllers
{
    /// <summary>
    /// Controladorr de productos
    /// </summary>
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        IProductManager productManager;
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="productManager">Manager de productos</param>
        public ProductController(IProductManager productManager)
        {
            this.productManager = productManager;
            
        }

        
        /// <summary>
        /// Obtiene todos los productos destacados
        /// </summary>
        /// <returns>Tupla con numero de productos y lista de productos destacados paginados</returns>
        [HttpGet]
        [Route("")]
        public Tuple<int, IEnumerable<ProductDTO>> GetFeaturedProduct(string orderby = "name", string orderdir="", int? take = int.MaxValue, int? skip = 0)
        {
            if (take == null)
                take = int.MaxValue;
            if (skip == null)
                skip = 0;

            var result = productManager.GetAll().Include(e=> e.Documents).Where(e => e.Featured || (e.Discount != null && e.Discount > 0));
            var count = result.Count();
            var list = result.Paginate(orderby, orderdir, (int)take, (int)skip);

            return new Tuple<int, IEnumerable<ProductDTO>>(
                count,
                list.ToList().AsQueryable().ProjectTo<ProductDTO>()
            );
        }

        /// <summary>
        /// Retorna un producto por si id
        /// </summary>
        /// <param name="id"> Identificador</param>
        /// <returns>Producto</returns>
        [HttpGet]
        [Route("{id}")]
        public ProductDTO Product(int id)
        {
            return Mapper.Map<ProductDTO>(productManager.GetById(id));
        }
        
        /// <summary>
        /// Añade un nuevo producto
        /// </summary>
        /// <param name="product">Producto a añadir</param>
        /// <returns>Nuevo id</returns>
        [HttpPut]
        [Route("Add")]
        public int Add(ProductCreateDTO product)
        {
            try
            {
                var newproduct = Mapper.Map<Product>(product);

                if (newproduct.Categories!=null && newproduct.Categories.Any())
                {
                    foreach ( var category in newproduct.Categories)
                    {
                        productManager.Context.Entry<Category>(category).State = EntityState.Unchanged;
                    }
                }
                productManager.Add(newproduct);
                productManager.SaveChanges();
                Write.Info("Se ha creado el producto " + product.Id);
                return newproduct.Id;
            }
            catch(Exception ex)
            {
                Write.Error(ex.Message, ex);
                throw ex;
            }
        }
    }
}
