﻿using AutoMapper.QueryableExtensions;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop.API.Controllers
{
    /// <summary>
    /// Controlador de categorias
    /// </summary>
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiController
    {
        ICategoryManager categoryManager = null;
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="categoryManager">Manager de categorias</param>
        public CategoryController(ICategoryManager categoryManager)
        {
            this.categoryManager = categoryManager;
        }

        /// <summary>
        /// Obtiene todas las categorias
        /// </summary>
        /// <returns>Todas las categorias</returns>
        [Route("")]
        [HttpGet]
        public IEnumerable<CategoryDTO> GetAll()
        {
            return categoryManager.GetAll().ProjectTo<CategoryDTO>();
        }
    }
}
