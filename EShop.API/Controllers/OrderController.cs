﻿using AutoMapper.QueryableExtensions;
using EShop.CORE.Interfaces;
using EShop.DTOs;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EShop.API.Controllers
{
    [RoutePrefix("api/Order")]
    public class OrderController : ApiController
    {
        IOrderManager orderManager;

        public OrderController (IOrderManager orderManager)
        {
            this.orderManager = orderManager;
        }

        /// <summary>
        /// Metodo para añadir una linea de producto al pedido.
        /// </summary>
        /// <param name="productId">Id del producto</param>
        /// <param name="quantity">Cantidad del producto</param>
        /// <param name="sessionId">Id de sesion del usuario</param>
        /// <returns>Elementos del pedido</returns>
        [HttpPost]
        [Route("AddProduct/{productId}/{quantity}/{sessionIid}")]
        public decimal AddProduct(int productId, int quantity, string sessionId)
        {
            return orderManager.AddProduct(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId,
                productId,
                quantity
                );
        }

        /// <summary>
        /// Obtiene los pedidos del usuario.
        /// </summary>
        /// <param name="sessionId">Id de sesion del usuario</param>
        /// <returns>Coleccion de pedidos del usuario</returns>
        [HttpGet]
        [Route("Order/{sessionId}")]
        public IEnumerable<OrderDTO> GetCart(string sessionId)
        {
            return orderManager.GetShoppingCartByUser(
                User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null,
                sessionId).ToList().AsQueryable().ProjectTo<OrderDTO>().ToList();
        }
    }
}
