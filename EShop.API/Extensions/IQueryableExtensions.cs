﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace EShop.API.Extensions
{
    /// <summary>
    /// Metodos extensibles para Iqueryable
    /// </summary>
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Metodo para paginar el Iqueryable
        /// </summary>
        /// <typeparam name="T"> Tipo de objeto del IQueryable</typeparam>
        /// <param name="this">Objeto para paginar</param>
        /// <param name="orderby">Campo por el que queremos ordenar</param>
        /// <param name="orderdir">Direccion de ordenacion asc o desc</param>
        /// <param name="take">Numero de elementos a coger</param>
        /// <param name="skip">Numero de elementos a saltar</param>
        /// <returns></returns>
        public static IQueryable<T> Paginate<T>
            (this IQueryable<T> @this, string orderby, string orderdir = "", int take = int.MaxValue, int skip=0)
        {
            //Si no me viene el campo por el que ordenar
            if (string.IsNullOrWhiteSpace(orderby))
                return @this;

            if (string.IsNullOrWhiteSpace(orderdir) || orderdir == "asc")
                orderdir = "";

            @this = @this.OrderBy(orderby + " " + orderdir);
            @this = @this.Skip(skip);
            @this = @this.Take(take);

            return @this;
        }
    }
}