﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop.MVC.Areas.Admin.Models.ProductAdmin
{
    /// <summary>
    /// View model de deicion de producto
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre del producto
        /// </summary>
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [MinLength(3, ErrorMessage ="El nombre ha de tener minimo 3 caracteres")]
        public string Name { get; set; }

        /// <summary>
        /// Descripcion corta
        /// </summary>
        [Display(Name = "Descripcion corta")]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        [Display(Name = "Precio")]
        [Required(ErrorMessage = "El precio es requerido")]
        [Range(0,int.MaxValue)]
        public decimal Pice { get; set; }

        /// <summary>
        /// Stock del producto
        /// </summary>
        [Display(Name = "Stock")]
        [Required(ErrorMessage = "El precio es requerido")]
        [Range(0,int.MaxValue)]
        public decimal Stock { get; set; }

        /// <summary>
        /// Disponible o no
        /// </summary>
        [Display(Name = "Disponible")]
        public bool Available { get; set; }

        /// <summary>
        /// Iva del producto
        /// </summary>
        [Display(Name = "Iva")]
        [Required(ErrorMessage = "El Iva es requerido")]
        public int Tax_Id { get; set; }

        /// <summary>
        /// Lista de ivas
        /// </summary>
        public List<SelectListItem> Taxs { get; set; }

        /// <summary>
        /// Esta en oferta
        /// </summary>
        [Display(Name = "En oferta")]
        public bool Featured { get; set; }

        /// <summary>
        /// Descuento del producto
        /// </summary>
        [Display(Name = "Descuento")]
        public decimal? Discount { get; set; }

        public List<SelectListItem> Categories { get; set; }
        [Display(Name = "Categories")]
        public List<string> SelectedCategories { get; set; }
    }
}