﻿using EShop.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EShop.MVC.Models.Product
{
    /// <summary>
    /// View model de product Index
    /// </summary>
    public class ProductIndexViewModel
    {
        /// <summary>
        /// Coleccion de productos
        /// </summary>
        public IEnumerable<ProductDTO> Products { get; set; }

        /// <summary>
        /// Coleccion de categorias
        /// </summary>
        public IEnumerable<CategoryDTO> Categories { get; set; }

        /// <summary>
        /// Numero total de productos a mostrar
        /// </summary>
        public int ProductCount { get; set; }


    }
}