﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EShop.MVC.Models
{
    public class LoginViewModel
    {
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        [Required]
        [Display(Name = "Correo electronico")]
        [EmailAddress]
        public string UserName { get; set; }

        /// <summary>
        /// Contraseña de usuarii
        /// </summary>
        [Required]
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}