﻿using EShop.MVC.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EShop.MVC.Models.Cart
{
    /// <summary>
    /// Modelo de vista de linea de carrito
    /// </summary>
    public class CartLineViewModel
    {

            /// <summary>
            /// Identificador de las lineas de carrito
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Nombre del producto
            /// </summary>
            [Display(ResourceType =typeof(Translations), Name ="Name")]
            public string Name { get; set; }

            /// <summary>
            /// CantidAD
            /// </summary>
            [Display(ResourceType = typeof(Translations), Name = "Quantity")]
            public decimal Quantity { get; set; }

            /// <summary>
            /// Precio unidad con impuestos incluidos
            /// </summary>
            [Display(ResourceType = typeof(Translations), Name  = "Price")]
            public decimal Price { get; set; }
        }
}