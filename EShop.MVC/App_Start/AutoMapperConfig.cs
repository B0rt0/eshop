﻿using AutoMapper;
using EShop.DTOs;
using EShop.MVC.Models.Cart;
using EShop.MVC.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop.MVC
{
    public  class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ProductDTO, ProductIndexViewModel>();
                cfg.CreateMap<ShoppingCartLinesDTO, CartLineViewModel>();

                //Admin
                cfg.CreateMap<ProductDTO, Areas.Admin.Models.ProductAdmin.ProductViewModel>();
                cfg.CreateMap<Areas.Admin.Models.ProductAdmin.ProductViewModel, ProductDTO>();
                cfg.CreateMap<Areas.Admin.Models.ProductAdmin.ProductViewModel, ProductCreateDTO>();
            });   
        }

    }
}