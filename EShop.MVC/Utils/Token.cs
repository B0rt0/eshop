﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop.MVC.Utils
{
    /// <summary>
    /// Clase token obtenido por la api
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Tiempo de expiracion del token
        /// </summary>
        public string expires_in { get; set; }

        /// <summary>
        /// Token de acceso
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Roles de usuario
        /// </summary>
        public string Roles { get; set; }
    }
}