﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop.MVC.Constants
{
    /// <summary>
    /// Clase estatica con los metodos del api
    /// </summary>
    public static class ApiEndPoints
    {

        /// <summary>
        /// Clase estatica con metosos api
        /// </summary>
        public const string GetToken = "{0}/Token";

        /// <summary>
        /// Url Para obtener los productos
        /// </summary>
        public const string GetProducts = "/api/Product?orderby={0}&orderdir={1}&take={2}&skip={3}";

        /// <summary>
        /// URL para obtener categorias
        /// </summary>
        public const string GetCategoryes = "/api/Category";

        /// <summary>
        /// Url para añadir un producto al carrito
        /// </summary>
        public const string PostProduct = "/api/Shop/AddProduct/{0}/{1}/{2}";

        /// <summary>
        /// URL para obtener el carrito
        /// </summary>
        public const string GetShoppingCartLines = "/api/shop/Cart/{0}";

        /// <summary>
        /// Url para eliminar
        /// </summary>
        public const string DeleteCartLine = "/api/Shop/Cart/{0}/{1}";

        /// <summary>
        /// Url para obtener producto
        /// </summary>
        public const string GetProduct = "/api/Product/{0}";

        /// <summary>
        /// Url para todas las tasas
        /// </summary>
        public const string GetAllTax = "/api/Tax";

        /// <summary>
        /// Url para agregar producto 
        /// </summary>
        public const string AddProduct = "/api/Product/Add";
    }
}