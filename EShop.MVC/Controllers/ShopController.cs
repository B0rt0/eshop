﻿
using AutoMapper.QueryableExtensions;
using EShop.DTOs;
using EShop.MVC.Constants;
using EShop.MVC.Models.Cart;
using EShop.MVC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop.MVC.Controllers
{
    /// <summary>
    /// Controlador de tienda
    /// </summary>
    public class ShopController : BaseController
    {
    
        /// <summary>
        /// Añade un producto al carrito
        /// </summary>
        /// <param name="productId">Identificador del producto</param>
        /// <returns>Numero de productos que contiene el carrito</returns>
        public ActionResult AddCartItem(int productId, int quantity = 1)
        {
            var count = Connect.Post<decimal>(
                string.Format(ApiEndPoints.PostProduct, productId, quantity, Session.SessionID),
                null
            );
            return Content(count.ToString());
        }

        /// <summary>
        /// Carrito del usuario
        /// </summary>
        /// <returns>Carrito</returns>
        public ActionResult GetCart()
        {
            var shoppingCartLines = Connect.Get<List<ShoppingCartLinesDTO>>(
                string.Format(ApiEndPoints.GetShoppingCartLines, Session.SessionID))
                .AsQueryable().ProjectTo<CartLineViewModel>();
            return Json(shoppingCartLines, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Cart()
        {
            var shoppingCartLines = Connect.Get<List<ShoppingCartLinesDTO>>(
               string.Format(ApiEndPoints.GetShoppingCartLines, Session.SessionID))
               .AsQueryable().ProjectTo<CartLineViewModel>();
            return View(shoppingCartLines);
        }

        /// <summary>
        /// Elimina una linea de carrito
        /// </summary>
        /// <param name="id">Id de la linea</param>
        /// <returns>El carrito sin la linea</returns>
        public ActionResult Delete(int id)
        {
            Connect.Delete(string.Format(ApiEndPoints.DeleteCartLine, id, Session.SessionID));
            return RedirectToAction("Cart");
        }
    }
}