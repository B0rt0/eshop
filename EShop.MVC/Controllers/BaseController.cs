﻿using EShop.MVC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EShop.MVC.Controllers
{
    public class BaseController : Controller
    {
        public Connect Connect { get; private set; }
        
        public BaseController()
        {
            Connect = new Connect();
        }
    }
}