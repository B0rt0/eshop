﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EShop.MVC.Security
{

        public interface IUserSession
        {
            string Username { get; }
            string BearerToken { get; }
            List<string> Roles { get; }
        }

    
}