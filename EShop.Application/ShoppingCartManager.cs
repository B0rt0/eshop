﻿using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Application
{
    /// <summary>
    /// Manager del carrito
    /// </summary>
    public class ShoppingCartManager : Manager<ShoppingCartLines>, IShoppingCartManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context"></param>
        public ShoppingCartManager(IApplicationDbContext context) : base(context)
        {
        }

        /// <inherutdoc/>
        public decimal AddProduct(string userId, string sessionId, int productId, decimal quantity)
        {
            var query = GetShoppingCartByUser(userId, sessionId).Where(e => e.ProductId == productId);
            if (query.Any())
            {
                query.Single().Quantity = query.Single().Quantity + quantity;
                query.Single().Date = DateTime.Now;
            }
            else
            {
                this.Context.ShoppingCart.Add(new ShoppingCartLines
                {
                    Date = DateTime.Now,
                    ProductId = productId,
                    Quantity = quantity,
                    UserId = userId,
                    Session = string.IsNullOrWhiteSpace(userId) ? sessionId : null
                });
            }
            this.Context.SaveChanges();
            return GetShoppingCartByUser(userId, sessionId).Sum(e => e.Quantity);
        }


        /// <inherutdoc/>
        public IQueryable<ShoppingCartLines> GetShoppingCartByUser (string userId, string sessionId)
        {
            var query = this.GetAll().Include(e=> e.Product);
            if (!string.IsNullOrWhiteSpace(userId))
            {
                query = query.Where(e => e.UserId == userId);
            }
            else
            {
                query = query.Where(e => e.Session == sessionId);
            }
            return query;
        }
    }
}
