﻿using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Application
{
    /// <summary>
    /// Manager de categorias
    /// </summary>
    public class CategoryManager : Manager<Category>, ICategoryManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public CategoryManager(IApplicationDbContext context) : base(context)
        {
        }
    }
}
