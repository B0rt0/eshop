﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EShop.CORE.Interfaces;
using EShop.CORE;

namespace EShop.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ProductManager : Manager<Product>, IProductManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ProductManager(IApplicationDbContext context) : base(context)
        {

        }
        
        //<Inhertitdoc/>
        public IQueryable<Product> GetByCategoryId(int categoryId)
        {
            return Context.Products.Where(p => p.Categories.Where(c => c.Id == categoryId).Any());
        }
    }
}
