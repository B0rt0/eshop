﻿using EShop.CORE.Domain;
using EShop.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Application
{
    /// <summary>
    /// Manager de pedidos
    /// </summary>
    public class OrderManager : Manager<Order>, IOrderManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public OrderManager(IApplicationDbContext context) : base(context)
        {
        }
        ///<Inhertitdoc/>
        public decimal AddProduct(string userId, string sessionId, int productId, decimal quantity)
        {
            throw new NotImplementedException();
        }
        ///<Inhertitdoc/>
        public IQueryable<Order> GetShoppingCartByUser(string userId, string sessionId)
        {
            throw new NotImplementedException();
        }
    }
}
