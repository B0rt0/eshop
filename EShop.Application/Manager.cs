﻿using EShop.CORE.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Application
{
    /// <summary>
    /// Clase generica de servicios de aplicacion
    /// </summary>
    public class Manager<T> : IManager<T>
        where T : class
    {
        /// <summary>
        /// Contexto de datos
        /// </summary>
        public IApplicationDbContext Context { get; private set; }

        /// <summary>
        /// Contructor de la clase
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public Manager(IApplicationDbContext context)
        {
            Context = context;
        }

        //<inheritdoc/>
        public T GetById(object id)
        {
            return Context.Set<T>().Find(id);
        }

        //<inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        //<inheritdoc/>
        public IEnumerable<T> GetByIds(List<Object> ids)
        {
            if (ids == null)
                return null;
            return ids.Select(e => GetById(e));
        }
        
        //<inheritdoc/>
        public void Remove(T element)
        {
            Context.Set<T>().Remove(element)
;        }


        //<inheritdoc/>
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        //<inheritdoc/>
        public void  Add(T element)
        {
            Context.Set<T>().Add(element);
        }
    }
}
